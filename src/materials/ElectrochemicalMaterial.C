/*************************************************************************
*
*  Welcome to Coral!
*  Andrea M. Jokisaari
*
*  27 June 2017
*
*************************************************************************/

#include "ElectrochemicalMaterial.h"

template<>
InputParameters validParams<ElectrochemicalMaterial>()
{
  InputParameters params = validParams<Material>();

  params.addRequiredParam<Real>("CH_mobility", "isotropic Cahn-Hilliard kinetic coefficient");
  params.addRequiredParam<Real>("kappa_CH", "gradient energy coefficient for c");

  params.addRequiredCoupledVar("concentration", "conserved variable");
  params.addRequiredCoupledVar("potential", "potential variable");

  params.addRequiredParam<Real>("c_alpha", "atomic fraction of alpha phase well");
  params.addRequiredParam<Real>("c_beta", "atomic fraction of beta phase well");
  params.addRequiredParam<Real>("w", "parameter controlling height of double well barrier");
  params.addRequiredParam<Real>("k", "charge on a solute");

  params.addRequiredParam<Real>("epsilon", "permittivity");

  return params;
}

ElectrochemicalMaterial::ElectrochemicalMaterial(const InputParameters & parameters) :
    Material(parameters),
    _M(declareProperty<Real>("CH_mobility")),
    _kappa_CH(declareProperty<Real>("kappa_CH")),

    _w(declareProperty<Real>("w")),
    _c_alpha(declareProperty<Real>("c_alpha")),
    _c_beta(declareProperty<Real>("c_beta")),

    _fbulk(declareProperty<Real>("f_bulk")),
    _dfbulkdc(declareProperty<Real>("dfbulk_dc")),
    _d2fbulkdc2(declareProperty<Real>("d2fbulk_dc2")),

    _felec(declareProperty<Real>("f_elec")),

    _k(declareProperty<Real>("k")),
    _epsilon(declareProperty<Real>("epsilon")),

    _M_param(getParam<Real>("CH_mobility")),
    _kappa_CH_param(getParam<Real>("kappa_CH")),

    _w_param(getParam<Real>("w")),
    _c_alpha_param(getParam<Real>("c_alpha")),
    _c_beta_param(getParam<Real>("c_beta")),
    _k_param(getParam<Real>("k")),
    _epsilon_param(getParam<Real>("epsilon")),

    _c(coupledValue("concentration")),
    _potential(coupledValue("potential"))
{
}

void
ElectrochemicalMaterial::computeQpProperties()
{
  _M[_qp] = _M_param;
  _kappa_CH[_qp] = _kappa_CH_param;

  _w[_qp] = _w_param;
  _c_alpha[_qp] = _c_alpha_param;
  _c_beta[_qp] = _c_beta_param;

  _k[_qp] = _k_param;
  _epsilon[_qp] = _epsilon_param;


  //chemical free energy
  _fbulk[_qp] = _w_param*( (_c[_qp] - _c_alpha_param)*(_c[_qp] - _c_alpha_param)*(_c_beta_param -_c[_qp])*(_c_beta_param -_c[_qp]));

  _dfbulkdc[_qp] = 2*_w_param*((_c[_qp] - _c_alpha_param)*(_c_beta_param -_c[_qp])*(_c_alpha_param + _c_beta_param -2*_c[_qp]));

  _d2fbulkdc2[_qp] = 2*_w_param*(6*_c[_qp]*_c[_qp] + _c_alpha_param*_c_alpha_param + _c_beta_param*_c_beta_param
                                 + 4*_c_alpha_param*_c_beta_param -6*_c[_qp]*(_c_alpha_param + _c_beta_param) );


  _felec[_qp] = 0.5*_k_param*_c[_qp]*_potential[_qp];

}
