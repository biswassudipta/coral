/******************************************************
 *
 *   Welcome to Coral!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   7 December 2016
 *
 *****************************************************/

#include "SolidificationMaterial.h"
#include "MooseMesh.h"

#include <cmath>

template<>
InputParameters validParams<SolidificationMaterial>()
{
  InputParameters params = validParams<Material>();

  params.addRequiredParam<Real>("theta_0", "offset azimuthal angle for interface anisotropy in radians");
  params.addRequiredParam<int>("m", "rotation order of crystal symmetry (integer)");
  params.addRequiredParam<Real>("epsilon", "anisotropy strength");
  params.addRequiredParam<Real>("tau_0", "time relaxation constant");
  params.addRequiredParam<Real>("D", "diffusion constant");
  params.addRequiredParam<Real>("W_0", "gradient coefficient constant");

  params.addRequiredCoupledVar("temperature","nondimensionalized temperature variable");
  params.addRequiredCoupledVar("order_parameter", "order parameter variable");

  return params;
}

SolidificationMaterial::SolidificationMaterial(const InputParameters & parameters) :
    Material(parameters),
    _theta_0(getParam<Real>("theta_0")),
    _m(getParam<int>("m")),
    _epsilon(getParam<Real>("epsilon")),
    _tau_0(getParam<Real>("tau_0")),
    _diffusion_coefficient(getParam<Real>("D")),
    _W_0(getParam<Real>("W_0")),

    _T(coupledValue("temperature")),
    _n(coupledValue("order_parameter")),

    _grad_n(coupledGradient("order_parameter")),

    _fbulk(declareProperty<Real>("f_bulk")),
    _dfbulk_dn(declareProperty<Real>("dfbulk_dn")),
    _d2fbulk_dn2(declareProperty<Real>("d2fbulk_dn2")),
    _fgrad(declareProperty<Real>("f_grad")),
    _dfgrad_dn_SI1(declareProperty<RealGradient>("dfgrad_dn_SI1")),
    _dfgrad_dn_SI2(declareProperty<RealGradient>("dfgrad_dn_SI2")),
    _tau(declareProperty<Real>("tau")),
    _D(declareProperty<Real>("D"))
{
  if(_mesh.dimension() > 2)
    mooseError("this problem is not formulated for 3 dimensions.");
}

void
SolidificationMaterial::computeQpProperties()
{
  _D[_qp] = _diffusion_coefficient;

  computeA();

  computeW();

  computeTau();

  computeBulkEnergy();

  computeGradientEnergy();

}

void
SolidificationMaterial::computeA()
{
  //tan(theta) = dn/dy / dn/dx
  Real theta = atan2( _grad_n[_qp](1), _grad_n[_qp](0) );

  _A = 1.0 + _epsilon*cos( double(_m)*(theta - _theta_0 ) );

}

void
SolidificationMaterial::computeW()
{
  _W = _A*_W_0;



  Real theta = atan2( _grad_n[_qp](1), _grad_n[_qp](0) );

  Real dw_part = _W_0*_epsilon*double(_m)*sin( double(_m)*( theta - _theta_0) );

  Real denominator =  _grad_n[_qp](0)*_grad_n[_qp](0) + _grad_n[_qp](1)* _grad_n[_qp](1);

  //threshold this so as to not blow up...picked somewhat arbitrarily...
  if(denominator < 1e-10)
    denominator = 1e-10;

  // _dW(0) = dW/( d (dn/dx) )
  _dW(0) = dw_part*_grad_n[_qp](1)/denominator;

  // _dW(1) = dW/( d (dn/dy) )
  _dW(1) = -dw_part*_grad_n[_qp](0)/denominator;

  //2D problem only!
  _dW(2) = 0;

}

void
SolidificationMaterial::computeTau()
{
  _tau[_qp] = _tau_0*_A*_A;

}

void
SolidificationMaterial::computeBulkEnergy()
{
  Real n2 = _n[_qp]*_n[_qp];

  Real lambda = _D[_qp]*_tau_0/( 0.6267*_W_0*_W_0 );

  _fbulk[_qp] = -0.5*n2 + 0.25*n2*n2 + lambda*_T[_qp]*_n[_qp]*( 1.0 - (2.0/3.0)*n2 + 0.2*n2*n2 );

  _dfbulk_dn[_qp] = (n2 - 1.0)*( _n[_qp] + lambda*_T[_qp]*(n2 - 1.0) );

  _d2fbulk_dn2[_qp] = 3.0*n2 - 1.0 + 4.0*lambda*_T[_qp]*_n[_qp]*(n2 - 1.0);

}

void
SolidificationMaterial::computeGradientEnergy()
{
  Real magnitude = sqrt( _grad_n[_qp](0)*_grad_n[_qp](0) + _grad_n[_qp](1)*_grad_n[_qp](1) );


  _fgrad[_qp] = 0.5*_W*_W*magnitude*magnitude;


  //this term is for the kernel computation, SolidificationInterface1
  _dfgrad_dn_SI1[_qp] = _W*_W*_grad_n[_qp];

  //this term is for the kernel computation, SolidificationInterface2
   _dfgrad_dn_SI2[_qp] = magnitude*magnitude*_W*_dW;

  //debug purposes
  // _dfgrad_dn_SI2[_qp] = magnitude*magnitude*_W;

}
