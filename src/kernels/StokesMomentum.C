/*************************************************************************
*
*  Welcome to Coral!
*  Andrea M. Jokisaari
*
*  23 June 2017
*
*************************************************************************/

#include "StokesMomentum.h"

template<>
InputParameters validParams<StokesMomentum>()
{
    InputParameters params = validParams<Kernel>();

  params.addClassDescription("This class computes the spatial component of the momentum equation"
                             "for incompressible Stokes flow.  It computes the residual and Jacobian.");
  params.addRequiredCoupledVar("p", "pressure");

  params.addRequiredParam<RealVectorValue>("gravity", "Direction of the gravity vector");
  params.addRequiredParam<unsigned>("component", "0, 1, 2 to indicate solving for the x, y, or z component of equation");
  params.addRequiredParam<Real>("mu", "value of the dynamic viscosity");
  params.addRequiredParam<Real>("rho", "value of the density");


  return params;
}

StokesMomentum::StokesMomentum(const InputParameters & parameters)
  : Kernel(parameters),
    _p(coupledValue("p")),
    _grad_p(coupledGradient("p")),
    _p_var_number(coupled("p")),
    _gravity(getParam<RealVectorValue>("gravity")),
    _component(getParam<unsigned>("component")),
//    _mu(getMaterialProperty<Real>("mu_name")),
//    _rho(getMaterialProperty<Real>("rho_name"))
    _mu(getParam<Real>("mu")),
    _rho(getParam<Real>("rho"))
{
}

Real
StokesMomentum::computeQpResidual()
{
  //here, we are tricky because we use "u", the variable given to the kernel
  //from the input file.  Do not think this is the u_component of velocity!
  Real viscous_part = _mu*(_grad_u[_qp]*_grad_test[_i][_qp]);

  Real pressure_part = _grad_p[_qp](_component) * _test[_i][_qp];

  Real body_force = - _rho*_test[_i][_qp]*_gravity(_component);

  return viscous_part + pressure_part + body_force;

}

Real
StokesMomentum::computeQpJacobian()
{
  Real viscous_part = _mu*(_grad_phi[_j][_qp]*_grad_test[_i][_qp]);

  //this really is zero
  Real pressure_part = 0;

  //this really is zero
  Real body_force = 0;

  return viscous_part + pressure_part + body_force;

}

Real
StokesMomentum::computeQpOffDiagJacobian(unsigned jvar)
{
  //this really is zero
  Real viscous_part = 0;

 //this really is zero
  Real body_force = 0;

  Real pressure_part = 0;

  //if it's for the pressure term, pressure_part =/= zero
  if (jvar == _p_var_number)
    return _grad_phi[_j][_qp](_component)*_test[_i][_qp];
  else
    return viscous_part + pressure_part + body_force;
}
