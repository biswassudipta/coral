
/******************************************************
 *
 *   Welcome to Coral!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   8 December 2016
 *
 *****************************************************/

#include "SolidificationDiffusion.h"

template<>
InputParameters validParams<SolidificationDiffusion>()
{
  InputParameters params =  validParams<Diffusion>();

  return params;
}

SolidificationDiffusion::SolidificationDiffusion(const InputParameters & parameters) :
    Diffusion(parameters),
    _D(getMaterialProperty<Real>("D"))
{
}

Real
SolidificationDiffusion::computeQpResidual()
{
  return _D[_qp]*Diffusion::computeQpResidual();
}

Real
SolidificationDiffusion::computeQpJacobian()
{
  return _D[_qp]*Diffusion::computeQpJacobian();
}
