/******************************************************
 *
 *   Welcome to Coral!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   4 January 2017
 *
 *****************************************************/

#include "GradientEnergy.h"

template<>
InputParameters validParams<GradientEnergy>()
{
  InputParameters params = validParams<AuxKernel>();

  return params;
}

GradientEnergy::GradientEnergy(const InputParameters & parameters) :
    AuxKernel(parameters),
    _fgrad(getMaterialProperty<Real>("f_grad"))
{
}

Real
GradientEnergy::computeValue()
{
  return _fgrad[_qp];

}
