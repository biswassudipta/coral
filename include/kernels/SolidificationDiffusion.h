/******************************************************
 *
 *   Welcome to Coral!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   8 December 2016
 *
 *****************************************************/

#ifndef SOLIDIFICATIONDIFFUSION_H
#define SOLIDIFICATIONDIFFUSION_H

#include "Diffusion.h"

class SolidificationDiffusion;

template<>
InputParameters validParams<SolidificationDiffusion>();

class SolidificationDiffusion : public Diffusion
{
public:
  SolidificationDiffusion(const InputParameters & parameters);

  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();

protected:

private:
  const MaterialProperty<Real> & _D;

};


#endif //SOLIDIFICATIONDIFFUSION_H
