/*************************************************************************
*
*  Welcome to Coral!
*  Andrea M. Jokisaari
*
*  23 June 2017
*
*************************************************************************/

#ifndef STOKESMOMENTUM_H
#define STOKESMOMENTUM_H

#include "Kernel.h"

class StokesMomentum;

template<>
InputParameters validParams<StokesMomentum>();

/**
 *This class computes the spatial component of the momentum equation
 *for incompressible Stokes flow.  It computes the residual and Jacobian.
 *Note that this code is heavily based on INSMomentumBase, INSMomentumLaplaceForm
 *from the Navier-Stokes MOOSE module, written by John Peterson.
 */

class StokesMomentum : public Kernel
{
public:
  StokesMomentum(const InputParameters & parameters);

  virtual ~StokesMomentum() {}

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();
  virtual Real computeQpOffDiagJacobian(unsigned jvar);

  const VariableValue & _p;

  // Gradients
  const VariableGradient & _grad_p;

  // Variable numberings
  unsigned _p_var_number;

  // Parameters
  RealVectorValue _gravity;
  unsigned _component;

  // Material properties
  //I think i'm going to be lazy and just supply these as constants to the kernel...
  //unless I need them to calculate other stuff?
  //const MaterialProperty<Real> & _mu;
  //const MaterialProperty<Real> & _rho;

  Real _mu;
  Real _rho;


private:

};


#endif //STOKESMOMENTUM_H
