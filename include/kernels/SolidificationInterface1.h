/******************************************************
 *
 *   Welcome to Coral!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   8 December 2016
 *
 *****************************************************/

#ifndef SOLIDIFICATIONINTERFACE1_H
#define SOLIDIFICATIONINTERFACE1_H

#include "Kernel.h"

class SolidificationInterface1;

template<>
InputParameters validParams<SolidificationInterface1>();

class SolidificationInterface1 : public Kernel
{
public:
  SolidificationInterface1(const InputParameters & parameters);

  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();

protected:

private:
  const MaterialProperty<RealGradient> & _dfgrad_dn_SI1;
  const MaterialProperty<Real> & _tau;

};


#endif //SOLIDIFICATIONINTERFACE1_H
