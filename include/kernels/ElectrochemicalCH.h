/*************************************************************************
*
*  Welcome to Coral!
*  Andrea M. Jokisaari
*
*  27 June 2017
*
*************************************************************************/

#ifndef ELECTROCHEMICALCH_H
#define ELECTROCHEMICALCH_H

#include "SplitCHCRes.h"

class ElectrochemicalCH;

template<>
InputParameters validParams<ElectrochemicalCH>();

class ElectrochemicalCH : public SplitCHCRes
{
public:
  ElectrochemicalCH(const InputParameters & parameters);

protected:
  virtual Real computeDFDC(PFFunctionType type);
  //virtual Real computeDEDC(PFFunctionType type);

  virtual Real computeQpOffDiagJacobian(unsigned jvar);

  //const MaterialProperty<Real> & _c_alpha;
  //const MaterialProperty<Real> & _c_beta;
  const MaterialProperty<Real> & _dfdc;
  const MaterialProperty<Real> & _d2fdc2;

//  const MaterialProperty<Real> & _w;
  const MaterialProperty<Real> & _k;

  const VariableValue & _potential;

  unsigned _potential_var_number;


private:
};

#endif //ELECTROCHEMICALCH_H
