/*************************************************************************
*
*  Welcome to Coral!
*  Andrea M. Jokisaari
*
*  23 June 2017
*
*************************************************************************/

#ifndef STOKESMASS_H
#define STOKESMASS_H

#include "Kernel.h"

class StokesMass;

template<>
InputParameters validParams<StokesMass>();

/**
 * This class computes the mass equation residual and Jacobian
 * contributions for the incompressible Stokes momentum
 * equation.  NB: code is heavily based on the INSMass kernel within
 * the Navier-Stokes MOOSE module (John Peterson).
 * Important!: Note that it takes pressure as the variable (I believe this is
 * because pressure would show up in the full formulation of this equation,
 * but it disappears due to the equation being for incompressible fluid.)
 */

class StokesMass : public Kernel
{
public:
  StokesMass(const InputParameters & parameters);

  virtual ~StokesMass() {}

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();
  virtual Real computeQpOffDiagJacobian(unsigned jvar);

  // Coupled Gradients
  const VariableGradient & _grad_u_vel;
  const VariableGradient & _grad_v_vel;
  const VariableGradient & _grad_w_vel;

  // Variable numberings
  unsigned _u_vel_var_number;
  unsigned _v_vel_var_number;
  unsigned _w_vel_var_number;
  unsigned _p_var_number;

private:
};

#endif //STOKESMASS_H
