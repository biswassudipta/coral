/******************************************************
 *
 *   Welcome to Coral!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   8 December 2016
 *
 *****************************************************/

#ifndef SOLIDIFICATIONINTERFACE2_H
#define SOLIDIFICATIONINTERFACE2_H

#include "Kernel.h"

class SolidificationInterface2;

template<>
InputParameters validParams<SolidificationInterface2>();

class SolidificationInterface2 : public Kernel
{
public:
  SolidificationInterface2(const InputParameters & parameters);

  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();

protected:

private:
  const MaterialProperty<RealGradient> & _dfgrad_dn_SI2;
  const MaterialProperty<Real> & _tau;

};


#endif //SOLIDIFICATIONINTERFACE2_H
