/******************************************************
 *
 *   Welcome to Coral!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   7 December 2016
 *
 *****************************************************/

#ifndef SOLIDIFICATIONBULK_H
#define SOLIDIFICATIONBULK_H

#include "Kernel.h"

class SolidificationBulk;

template<>
InputParameters validParams<SolidificationBulk>();

class SolidificationBulk : public Kernel
{
public:
  SolidificationBulk(const InputParameters & parameters);

  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();

protected:

private:
  const MaterialProperty<Real> & _dfdn;
  const MaterialProperty<Real> & _d2fdn2;
  const MaterialProperty<Real> & _tau;

};


#endif //SOLIDIFICATIONBULK_H
