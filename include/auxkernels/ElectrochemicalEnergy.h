/*************************************************************************
*
*  Welcome to Coral!
*  Andrea M. Jokisaari
*
*  7 July 2017
*
*************************************************************************/
#ifndef ELECTROCHEMICALENERGY_H
#define ELECTROCHEMICALENERGY_H

#include "AuxKernel.h"

//Forward Declarations
class ElectrochemicalEnergy;

template<>
InputParameters validParams<ElectrochemicalEnergy>();

class ElectrochemicalEnergy : public AuxKernel
{
public:
  ElectrochemicalEnergy(const InputParameters & parameters);

protected:
  virtual Real computeValue();

private:
  const MaterialProperty<Real> & _fchem;
  const MaterialProperty<Real> & _felec;
  const MaterialProperty<Real> & _kappa_CH;

  const VariableGradient & _grad_c;

};

#endif //BENCHMARKICHENERGY_H
