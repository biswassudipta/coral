/******************************************************
 *
 *   Welcome to Coral!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   4 January 2017
 *
 *****************************************************/

#ifndef GRADIENTENERGY_H
#define GRADIENTENERGY_H

#include "AuxKernel.h"

//forward declarations
class GradientEnergy;


template<>
InputParameters validParams<GradientEnergy>();

class GradientEnergy : public AuxKernel
{
public:
  GradientEnergy(const InputParameters & parameters);

protected:
  virtual Real computeValue();

private:
  const MaterialProperty<Real> & _fgrad;
};

#endif //GRADIENTENERGY_H
