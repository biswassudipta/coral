/******************************************************
 *
 *   Welcome to Hackathon!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   19 February 2016
 *
 *****************************************************/

#ifndef BENCHMARKICONCIC_H
#define BENCHMARKICONCIC_H

#include "InitialCondition.h"

// Forward Declarations
class BenchmarkIConcIC;
namespace libMesh { class Point; }

template<>
InputParameters validParams<BenchmarkIConcIC>();

class BenchmarkIConcIC : public InitialCondition
{
public:
  BenchmarkIConcIC(const InputParameters & parameters);
  virtual Real value(const Point & p);

protected:

private:
  Real _epsilon;
  Real _base_value;

  Point _q1;
  Point _q2;
  Point _q3;
  Point _q4;

  Real _k;
};

#endif //BENCHMARKICONCIC_H
