#This is an input file for Benchmark II problem set, dendritic growth.

[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 75
  ny = 75
  xmin = 0
  xmax = 960
  ymin = 0
  ymax = 960

  elem_type = QUAD4
[]

#[GlobalParams]
#[]

[Variables]
  #phase field variable
  [./n]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = SmoothCircleIC
      invalue = 1
      outvalue = -1
      radius = 8
      int_width = 1
      x1 = 0
      y1 = 0
    [../]
  [../]

  #nondimensional temperature
  [./u]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = ConstantIC
      value = -0.3
    [../]
  [../]
[]

[AuxVariables]
  [./bulk_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]

[./gradient_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]

[./total_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]
[]

[Preconditioning]
#  [./SMP]
#    type = SMP
#    full = true
#  [../]
[]

[Kernels]
  [./dndt]
    type = TimeDerivative
    variable = n
  [../]

  [./bulk_energy]
    type = SolidificationBulk
    variable = n
  [../]

  [./int_energy_1]
    type = SolidificationInterface1
    variable = n
  [../]

  [./int_energy_2]
    type = SolidificationInterface2
    variable = n
  [../]

  [./dudt]
    type = TimeDerivative
    variable = u
  [../]

  [./diffusion]
    type = SolidificationDiffusion
    variable = u
  [../]

  [./source_term]
    type = SolidificationSourceTerm
    variable = u
    v = n
  [../]
[]

[AuxKernels]
  [./total_energy_calc]
      type = TotalEnergy
      variable = total_energy
  [../]

  [./bulk_energy_calc]
      type = BulkEnergy
      variable = bulk_energy
  [../]

  [./gradient_energy_calc]
      type = GradientEnergy
      variable = gradient_energy
  [../]
[]

[Materials]
  [./system]
    type = SolidificationMaterial

    Tm = 0 #this needs to be defined in the benchmark problem for completeness
    Cp = 1 #this needs to be defined in the benchmark problem for completeness
    latent_heat = 1 #this needs to be defined in the benchmark problem for completeness

    theta_0 = 0 #0.7853975
    m = 4
    epsilon = 0.05
    tau_0 = 1
    D = 10
    W_0 = 1

    temperature = u
    order_parameter = n
  [../]
[]

[BCs]
#  [./temp_bc]
#    type = DirichletBC
#    value = -0.3
#    variable = u
#    boundary = '0 1 2 3'
#  [../]
[]

[Postprocessors]
  [./numDOFs]
    type = NumDOFs
  [../]
  [./TotalEnergy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]
  [./dt]
    type = TimestepSize
  [../]
  [./NL_iter]
    type = NumNonlinearIterations
  [../]
  [./Volume]
    type = VolumePostprocessor
    execute_on = initial
  [../]
  [./feature_counter]
    type = FeatureFloodCount
    variable = n
    threshold = 0
    compute_var_to_feature_map = true
    use_displaced_mesh = false
    execute_on = timestep_end
  [../]
  [./VolumeFraction]
    type = FeatureVolumeFraction
    mesh_volume = Volume
    feature_volumes = feature_volumes
    use_displaced_mesh = false
    execute_on = timestep_end
  [../]
[]

[VectorPostprocessors]
  [./feature_volumes]
    type = FeatureVolumeVectorPostprocessor
    flood_counter = feature_counter
    outputs = none
    execute_on = timestep_end
  [../]
[]

[Adaptivity]
  initial_steps = 5
  max_h_level = 5
  initial_marker = EFM_1
  marker = combo
 [./Markers]
    [./EFM_1]
      type = ErrorFractionMarker
      coarsen = 0.02
      refine = 0.75
      indicator = GJI_1
    [../]
    [./EFM_2]
      type = ErrorFractionMarker
      coarsen = 0.02
      refine = 0.75
      indicator = GJI_2
    [../]
    [./combo]
      type = ComboMarker
      markers = 'EFM_1 EFM_2'
    [../]
  [../]

  [./Indicators]
    [./GJI_1]
     type = GradientJumpIndicator
     variable = n
    [../]
   [./GJI_2]
     type = GradientJumpIndicator
     variable = u
    [../]
  [../]
[]

[Executioner]
  type = Transient
  scheme = bdf2
#  scheme = crank-nicolson
#  scheme = dirk
#  scheme = rk-2

  [./TimeStepper]
    type = IterationAdaptiveDT
    dt = 0.003
    cutback_factor = 0.95
    growth_factor = 1.05
    optimal_iterations = 4
    iteration_window = 0
    linear_iteration_ratio = 100
  [../]

  #num_steps = 5000
end_time = 1501
  dtmin = 0.003
  dtmax = 0.3

  solve_type = 'PJFNK'
  petsc_options_iname = '-pc_type -sub_pc_type -sub_ksp_type -pc_asm_overlap'
  petsc_options_value = ' asm      lu           preonly       1'

  timestep_tolerance = 1e-5

  l_max_its = 100
 # nl_max_its = 10
  nl_abs_tol = 1e-11

  #trans_ss_check = 1
  #ss_check_tol = 5e-8
[]

[Outputs]
  exodus = true
  interval = 50
  csv = true
  file_base = e005_D10_BDF2_dt0p3

  checkpoint = true
  sync_times = '15 75 150 300 600 900 1200 1500'

[./console]
    type = Console
    perf_log = true
  [../]
[]