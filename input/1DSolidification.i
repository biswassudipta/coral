#This is an input file for Hackathon 2015 problem 2.

[Mesh]
  type = GeneratedMesh
  dim = 1
  nx = 50
  ny = 50
  xmin = 0
  xmax = 300
  ymin = 0
  ymax = 300

#  elem_type = QUAD4
[]

#[GlobalParams]
#[]

[Variables]
  #phase field variable
  [./n]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = SmoothCircleIC
      invalue = 1
      outvalue = -1
      radius = 5
      int_width = 5
      x1 = 0
      y1 = 0
    [../]
  [../]

  #nondimensional temperature
  [./u]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = ConstantIC
      value = -0.3
      #type = RandomIC
      #min = -0.05
      #max = -0.0499
    [../]
  [../]
[]

[AuxVariables]
#  [./total_energy]
#    order = CONSTANT
#    family = MONOMIAL
#  [../]
[]

[Preconditioning]
#  [./SMP]
#    type = SMP
#    full = true
#  [../]
[]

[Kernels]
  [./dndt]
    type = TimeDerivative
    variable = n
  [../]

  [./bulk_energy]
    type = SolidificationBulk
    variable = n
  [../]

  [./int_energy_1]
    type = SolidificationInterface1
    variable = n
  [../]

  [./int_energy_2]
    type = SolidificationInterface2
    variable = n
  [../]

  [./dudt]
    type = TimeDerivative
    variable = u
  [../]

  [./diffusion]
    type = SolidificationDiffusion
    variable = u
  [../]

  [./source_term]
    type = SolidificationSourceTerm
    variable = u
    v = n
  [../]
[]

[AuxKernels]
#   [./total_energy_calc]
#      type = HackathonCHACEnergy
#      variable = total_energy
#      CH_var = c
#      OP_var_names = 'n1 n2 n3 n4'
   [../]
[]

[Materials]
  [./system]
    type = SolidificationMaterial

    Tm = 0 #this needs to be defined in the benchmark problem for completeness
    Cp = 1 #this needs to be defined in the benchmark problem for completeness
    latent_heat = 1 #this needs to be defined in the benchmark problem for completeness

    theta_0 = 0 #0.7853975
    m = 4
    epsilon = 0.05
    tau_0 = 1
    D = 10 #15 #25 #20 #10  #50
    W_0 = 1

    temperature = u
    order_parameter = n
  [../]
[]

[BCs]
  [./temp_bc]
    type = DirichletBC
    value = -0.3
    variable = u
    boundary = '1'
  [../]
[]

[Postprocessors]
  [./numDOFs]
    type = NumDOFs
  [../]
#  [./TotalEnergy]
#    type = ElementIntegralVariablePostprocessor
#    variable = total_energy
#  [../]
  [./dt]
    type = TimestepSize
  [../]
  [./NL_iter]
    type = NumNonlinearIterations
  [../]
  [./L_evals]
    type = NumResidualEvaluations
  [../]
  [./nodes]
    type = NumNodes
  [../]
[]

[Adaptivity]
  initial_steps = 4
  max_h_level = 4
  marker = combo
 [./Markers]
    [./EFM_1]
      type = ErrorFractionMarker
      coarsen = 0.02
      refine = 0.75
      indicator = GJI_1
    [../]
    [./EFM_2]
      type = ErrorFractionMarker
      coarsen = 0.02
      refine = 0.25
      indicator = GJI_2
    [../]
    [./combo]
      type = ComboMarker
      markers = 'EFM_1 EFM_2'
    [../]
  [../]

  [./Indicators]
    [./GJI_1]
     type = GradientJumpIndicator
     variable = n
    [../]
   [./GJI_2]
     type = GradientJumpIndicator
     variable = u
    [../]
  [../]
[]

[Executioner]
  type = Transient
#  scheme = bdf2
#  scheme = crank-nicolson
#  scheme = dirk
#   scheme = rk-2

#  [./TimeStepper]
#    type = SolutionTimeAdaptiveDT
#    dt = 1e-1
#    percent_change = 0.05
#  [../]

  [./TimeStepper]
    type = IterationAdaptiveDT
    dt = 1e-3
    cutback_factor = 0.75
    growth_factor = 1.05
    optimal_iterations = 5
    iteration_window = 1
    linear_iteration_ratio = 100
  [../]

  #num_steps = 5000
  end_time = 1000
  #dtmin = 1e-3

  solve_type = 'PJFNK'
  petsc_options_iname = '-pc_type -sub_pc_type -sub_ksp_type -pc_asm_overlap'
  petsc_options_value = ' asm      lu           preonly       1'

  timestep_tolerance = 1e-5

  l_max_its = 100
 # nl_max_its = 10
  nl_abs_tol = 1e-11

  #trans_ss_check = 1
  #ss_check_tol = 5e-8
[]

[Outputs]
  exodus = true
  interval = 10
  csv = true
  file_base = 1D_test

  checkpoint = false #true
  sync_times = '1 5 10 20 50 100 200 500'

  [./console]
    type = Console
    perf_log = true
  [../]
[]
